<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Шоколадный Шеогорат</title>
</head>
<style>
    body {
	color:#b9b9b9;
	font-size:16px;
	font-family: Comic Sans Ms;
	background-image: url('http://7829.selcdn.ru/sheogorath.blindage.org/chocolate%20sheogorath.jpg');
	background-position: top center;
	background-repeat: no-repeat;
	background-color: black;
    }
    a {
	color: #4a3436;
    }
    #content {
	background-color:black;
	display:block;
	position:relative;
	margin-left: auto;
	margin-right: auto;
	top: 420px;
	width:740px;
    }
    #content ul {
	list-style:none;
    }
    #content ul li {
	border: 1px solid #463023;
	padding: 10px;
	margin-bottom:20px;
    }
    #content ul li #postlink a {
	text-decoration: none;
    }
    #content ul li #postlink {
	font-size:11px;
	position:relative;
	bottom:0px;
	right:0px;
	border:1px solid #463023;
	
    }
</style>
<body>
<div id="content">
<ul>
<?

if (isset($_REQUEST['postid'])) { 
//если просят конкретную запись
$post_id=$_REQUEST['postid'];

$records_json=file_get_contents("https://api.vk.com/method/wall.getById?posts=$post_id&extended=0");
$records=json_decode($records_json, true);

foreach($records['response'] as $record) {
    echo "<li id='".$record['id']."'>".$record['text']."";
    if (isset($record['attachment']['photo']['src_big']))
    echo "<p><img src='".$record['attachment']['photo']['src_big']."'></p>";
    echo "</li>";
}
 
//конец кода получения конкретной записи
} else  {
//если смотрят стену

if (isset($_REQUEST['offset']))
$offset=$_REQUEST['offset']; 
else $offset=0;
$count=5;


$records_json=file_get_contents("https://api.vk.com/method/wall.get?owner_id=-62061002&domain=chocogorath&filter=owner&count=$count&offset=$offset");
$records=json_decode($records_json, true);

$count_all=$records['response'][0];

unset($records['response'][0]);
//var_dump($records);
foreach($records['response'] as $record) {
    echo "<li id='".$record['id']."'>".$record['text']."\n";
    if (isset($record['attachment']['photo']['src_big']))
    echo "<p><img src='".$record['attachment']['photo']['src_big']."'></p>\n";
    echo "<br><span align='right' id='postlink'><a href='?postid=".$record['from_id']."_".$record['id']."'>прямая ссылка</a></span>\n";
    echo "</li>\n\n";
}
?>
</ul>
<?
echo "<p>";

if ($count_all-($count+$offset)>0) {
$next_offset=$offset+$count;
echo "<a href='?offset=" . $next_offset . "'>";
echo "Следующая страница</a> ";
}

if (($count+$offset)>$count_all) {
$prev_offset=$offset-$count;
echo "<a href='?offset=" . $prev_offset . "'>";
echo "Предыдущая страница</a> ";
}
echo "</p>";

?>
<?
//конец кода просмотра стены
 } ?>
<p align="center">Copyright by <a href="http://blindage.org">Vladimir Smagin</a>, 2013.</p>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter25697486 = new Ya.Metrika({id:25697486, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/25697486" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</div>
</body>
</html>

